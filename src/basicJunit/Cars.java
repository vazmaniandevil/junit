package basicJunit;

public class Cars {

    private String Model;
    private boolean Available = false;
    private boolean ExtendedCab = true;
    private Integer StockQuantity = null;
    private Integer Year = 2002;
    private Integer Warranty = 5;


    public Cars(String model) {
        this.Model = model;
    }

    public String getModel() {
        return Model;
    }

    public boolean isAvailable() {
        return Available;
    }

    public boolean extendedCab() throws Exception {
        boolean i = ExtendedCab;

        while (!i) {
            throw new Exception("Extended Cab is not available");
        }
        return true;
    }

    public Integer stockQuantity() {
        return StockQuantity;
    }

    public Integer carYear() throws Exception {
        boolean i = Year < 2000;

        if (i == true){
            throw new Exception("Car is really old!");
        }
        return Year;
    }

    public Integer remainingWarranty() throws Exception {
        int  i = Warranty - 3;
        if (i == 0) {
            throw new Exception("No more warranty!");
        }
        else {
            return i;
        }
    }

}


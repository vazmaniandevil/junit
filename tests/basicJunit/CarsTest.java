package basicJunit;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class CarsTest {

   private Cars model = new Cars("Ranger");
   private Cars modelTwo = new Cars("Ranger");

    @Test
    public void testEquals() {
        assertEquals("Ranger", model.getModel());
    }

    @Test
    public void testFalse() {
        assertFalse(model.isAvailable());
    }

    @Test
    public void testTrue() throws Exception {
        model.extendedCab();
        assertTrue(model.extendedCab());
    }

    @Test
    public void testNull() {
        model.stockQuantity();
        assertNull(model.stockQuantity());
    }

    @Test
    public void testNotSame() {
        assertNotSame(model, modelTwo);
    }

    @Test
    public void testNotNull() throws Exception {
        model.stockQuantity();
        assertNotNull(model.carYear());
    }

    @Test
    public void testSame() {
        assertSame(model, model);
    }

    @Test
    public void testThat() throws Exception {
        model.remainingWarranty();
        assertThat(model.remainingWarranty(), is(2));
    }

    /*
    * assertEquals OK
    * assertFalse OK
    * assertNotNull OK
    * assertNotSame PK
    * assertNull OK
    * assertSame OK
    * assertThat OK
    * assertTrue OK
    */

}